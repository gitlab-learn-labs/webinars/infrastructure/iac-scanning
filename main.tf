
# no versioning
resource "aws_s3_bucket" "b" {
  bucket  = "Bucket-sample"
}

data "aws_vpc" "main" {
}

resource "aws_security_group" "allow_20" {
  name        = "allow_20"
  description = "Allow all on port 20"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    description      = "To Open"
    from_port        = 20
    to_port          = 20
    protocol         = "tcp"
     cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}